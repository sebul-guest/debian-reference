#######################################################################
# Build debian-reference (v2)
# vim: set ts=8:
#######################################################################
#======================================================================
# ===== Adjustable parameters =========================================
#======================================================================
# base file name excluding file extension
MANUAL	:=	debian-reference
# languages translated with PO files
# This can list more than debian/rules for translator to check the result
LANGPO	:=	ja fr it pt de zh-cn zh-tw es id nb sv fi pt-br
# languages to skip generation of PDF files (not used now)
NOPDF	:=
# languages to build document
LANGALL	=	en $(LANGPO)

# Packages uploaded to unstable repo generate official Debian web pages
# Change these to post-release state when Debian is under soft freeze
#
# current "stable" release name
RELEASE_NAME0L := bullseye
RELEASE_NAME0C := Bullseye
# next "stable" / "testing" release name
RELEASE_NAME1L := bookworm
RELEASE_NAME1C := Bookworm
# next next "stable" / next "testing" release name
RELEASE_NAME2L := trixie
RELEASE_NAME2C := Trixie
# release name considered outdated (Release_Date-EOL_for_LTS)
RELEASE_NAMEXX := Jessie 8 (2015-2020)
# release names when this document was last updated
RELEASE_DOC_1  := Bullseye (=<literal>stable</literal>)
RELEASE_DOC_2  := Bookworm (=<literal>testing</literal>)

# version in changelog
DR_VERSION :=	$(shell dpkg-parsechangelog --show-field Version)
# set SOURCE_DATE_EPOCH if unset (when not called from debhelper)
SOURCE_DATE_EPOCH ?= $(shell dpkg-parsechangelog -STimestamp)
# date in changelog
SOURCE_DATE := $(shell date -u +'%F %T %Z' --date=@$(SOURCE_DATE_EPOCH))

# BUILD_DIR is not mean to be set to generic TMPDIR
BUILD_DIR	:= $(CURDIR)/build

# Change $(DRAFTMODE) from "yes" to "maybe" when this document
# should go into production mode
#DRAFTMODE      := yes
DRAFTMODE       := maybe
export DRAFTMODE
# Prevent external TZ export to impact build result
TZ	:= "0"
export TZ
# for reproducible build
FORCE_SOURCE_DATE = 1
export FORCE_SOURCE_DATE
#######################################################################
### basic constant parameters
#######################################################################
# Directories (no trailing slash)
DXSL	:=	xslt
DBIN	:=	bin
DRAW	:=	rawxml
DPO	:=	po
DIMG	:=	/usr/share/xml/docbook/stylesheet/nwalsh/images

# work around https://bugs.debian.org/725931
PERL_PERTURB_KEYS := 0
export PERL_PERTURB_KEYS
PERL_HASH_SEED := 0
export PERL_HASH_SEE

# Program name and option
XLINT	:=	xmllint --format
XPNO	:=	xsltproc --novalid --nonet
XPINC	:=	/usr/bin/faketime @$(SOURCE_DATE_EPOCH) xsltproc --novalid --nonet --xinclude
GETTEXT	:=	po4a-gettextize -M utf-8 -L utf-8 --format docbook
UPDATEPO:=	msgmerge --update --previous
MSGATTR	:=	msgattrib
MSGCAT	:=	msgcat
MSGUNIQ	:=	msguniq --use-first
MSGNOOBS:=      msgattrib --no-obsolete
DBLATEX	:=	/usr/bin/faketime @$(SOURCE_DATE_EPOCH) /usr/bin/dblatex
SED	:=	sed
ZIP	:=      /usr/bin/faketime @$(SOURCE_DATE_EPOCH) zip

# Source files
SRC_XML :=	$(wildcard $(DRAW)/*_*.rawxml)
SRC_ENT:=	remote.ent common.ent
# source PO files for all languages (build process requires these)
SRC_PO :=	$(addsuffix .po, $(addprefix  $(DPO)/, $(LANGPO)))
# sed script for replace
SEDF ?= $(DBIN)/replace0

# Remote file source (related to Debian package archive URLs)
#DEBM	:=	http://ftp.us.debian.org/debian/dists
#DEBM	:=	http://ftp.jp.debian.org/debian/dists
DEBM	:=	http://deb.debian.org/debian/dists
# Debian popcon data source URL
UPOPC	:=	https://popcon.debian.org/all-popcon-results.txt.gz
# Debian release name and arch used
CODE	:=	sid
ARCH	:=	amd64
UDEBA	:=	$(DEBM)/$(CODE)
UDEBB	:=	$(DEBM)/experimental
UDEBC	:=	$(DEBM)/stable
UDEBD	:=	$(DEBM)/oldstable
# processed remote data
PACKAGES_DATA := packages.main packages.contrib packages.non-free \
		packages.txt packages.experimental.txt \
		packages.stable.txt packages.oldstable.txt
POPCON_DATA :=  all-popcon-results.txt \
		all-popcon-date.txt \
		all-popcon-submissions.txt \
		all-popcon-pkgs.txt
PKG_LIST :=	pkg.lst
REMOTE_DATA :=	$(PACKAGES_DATA) $(POPCON_DATA)


#######################################################################
# Used as $(call check-command, <command>, <package>)
define check-command
set -e; if ! type $(1) >/dev/null; then \
  echo "Missing command: $(1), install package: $(2)"; \
  false; \
fi
endef
# Used as $(call check-file, <file>)
define check-file
set -e; if ! [ -r $(1) ]; then \
  echo "Missing file: $(1)"; \
  false; \
fi
endef
#======================================================================
# ===== Build targets =================================================
#======================================================================
#######################################################################
# $ make all       # build all
#######################################################################
.PHONY: all
# set LANGPO to limit language to speed up build
all: css html txt pdf
	cat fuzzy.log

#######################################################################
# $ make test      # build html for testing (for Translator)
#######################################################################
.PHONY: test
test: html css
	cat fuzzy.log

#######################################################################
# $ make entity    # refresh remote.ent (Need remote access)
#######################################################################
# For major English update, run this and commit updated data
.PHONY: entity
entity:
	$(MAKE) entityclean
	$(MAKE) remote.ent
#######################################################################
# $ make en      # build html for testing (for English)
#######################################################################
.PHONY: en
en:
	$(MAKE) entity
	$(MAKE) html css LANGPO=

#######################################################################
# Phase 1 (pre-build): Debian archive stat data
#######################################################################
.PHONY: remote
remote: $(REMOTE_DATA)

packages.main:
	# FETCH PACKAGE (sid main)
	@$(call check-command, wget, wget)
	@$(call check-command, grep-dctrl, dctrl-tools)
	wget -O - $(UDEBA)/main/binary-$(ARCH)/Packages.xz      | xzcat - > packages.main.tmp
	grep-dctrl -e -sPackage,Installed-Size -P "." packages.main.tmp > packages.main
	rm packages.main.tmp

packages.contrib:
	# FETCH PACKAGE (sid contrib)
	@$(call check-command, wget, wget)
	@$(call check-command, grep-dctrl, dctrl-tools)
	wget -O - $(UDEBA)/contrib/binary-$(ARCH)/Packages.xz   | xzcat - > packages.contrib.tmp
	grep-dctrl -e -sPackage,Installed-Size -P "." packages.contrib.tmp > packages.contrib
	rm packages.contrib.tmp

packages.non-free:
	# FETCH PACKAGE (sid non-free)
	@$(call check-command, wget, wget)
	@$(call check-command, grep-dctrl, dctrl-tools)
	wget -O - $(UDEBA)/non-free/binary-$(ARCH)/Packages.xz  | xzcat - > packages.non-free.tmp
	grep-dctrl -e -sPackage,Installed-Size -P "." packages.non-free.tmp > packages.non-free
	rm packages.non-free.tmp

packages.experimental.txt:
	# FETCH PACKAGE (experimental main)
	@$(call check-command, wget, wget)
	@$(call check-command, grep-dctrl, dctrl-tools)
	wget -O - $(UDEBB)/main/binary-$(ARCH)/Packages.xz      | xzcat - > packages.experimental.tmp
	grep-dctrl -e -sPackage,Installed-Size -P "." packages.experimental.tmp > packages.experimental.txt
	rm packages.experimental.tmp

packages.txt: packages.main packages.contrib packages.non-free
	# FETCH PACKAGE (sid)
	cat packages.main packages.contrib packages.non-free >packages.txt

packages.stable.txt:
	# FETCH PACKAGE (stable main)
	@$(call check-command, wget, wget)
	@$(call check-command, grep-dctrl, dctrl-tools)
	wget -O - $(UDEBC)/main/binary-$(ARCH)/Packages.xz      | xzcat - > packages.stable.tmp
	grep-dctrl -e -sPackage,Installed-Size -P "." packages.stable.tmp > packages.stable.txt
	rm packages.stable.tmp

packages.oldstable.txt:
	# FETCH PACKAGE (oldstable main contrib)
	@$(call check-command, wget, wget)
	@$(call check-command, grep-dctrl, dctrl-tools)
	wget -O - $(UDEBD)/main/binary-$(ARCH)/Packages.xz      | xzcat - > packages.oldstable.tmp
	grep-dctrl -e -sPackage,Installed-Size -P "." packages.oldstable.tmp > packages.oldstable.txt
	rm packages.oldstable.tmp
	wget -O - $(UDEBD)/contrib/binary-$(ARCH)/Packages.xz      | xzcat - > packages.oldstable.tmp
	grep-dctrl -e -sPackage,Installed-Size -P "." packages.oldstable.tmp >> packages.oldstable.txt
	rm packages.oldstable.tmp

all-popcon-results.txt:
	# POPCON RESULTS
	wget -O - $(UPOPC) | zcat - > all-popcon-results.txt

all-popcon-submissions.txt: all-popcon-results.txt
	sed -n -e 's/^Submissions: *\([^ ]*\) *$$/\1/p' < all-popcon-results.txt > all-popcon-submissions.txt

all-popcon-date.txt: all-popcon-results.txt
	echo "<!ENTITY pop-date \"$(shell date -u +'%F %T %Z')\">"	>  all-popcon-date.txt

all-popcon-pkgs.txt: all-popcon-results.txt
	grep --binary-files=text -e '^Package: [a-z0-9][-+a-z0-9.]*[ 0-9]*$$' < all-popcon-results.txt > all-popcon-pkgs.txt

# RAWXML (with @emacs@ etc.) to PACKAGE LIST
pkg.lst: $(DRAW)/$(MANUAL).rawxml
	@$(call check-command, xsltproc, xsltproc)
	# PACKAGE LIST of packages mentioned in the source XML
	$(XPNO) $(DXSL)/pkg.xsl $(DRAW)/$(MANUAL).rawxml > pkg.lst

# ENTITY DATA
# source XML inclusion file (pre-commited since this needs remote access)
remote.ent:
	$(MAKE) $(REMOTE_DATA) $(PKG_LIST)
	cat all-popcon-date.txt													>  remote.ent
	echo "<!ENTITY all-packages \"$$(grep -e '^Package:' packages.txt | wc -l)\">"						>> remote.ent
	echo "<!ENTITY main-packages \"$$( grep -e '^Package:' packages.main | wc -l)\">"					>> remote.ent
	echo "<!ENTITY contrib-packages \"$$(grep -e '^Package:' packages.contrib | wc -l)\">"					>> remote.ent
	echo "<!ENTITY non-free-packages \"$$(grep -e '^Package:' packages.non-free | wc -l)\">"				>> remote.ent
	sort pkg.lst | uniq | $(DBIN)/sizeent packages.txt packages.experimental.txt packages.stable.txt packages.oldstable.txt	>> remote.ent
	echo "<!ENTITY pop-submissions \"$$(sed -n -e 's/^Submissions: *\([^ ]*\) *$$/\1/p' < all-popcon-results.txt)\">"	>> remote.ent
	echo "<!ENTITY pop-architectures \"$$(grep --binary-files=text -e '^Architecture:' all-popcon-results.txt | wc -l)\">"	>> remote.ent
	echo "<!ENTITY pop-packages \"$$(grep -e '^Package:' all-popcon-pkgs.txt | wc -l)\">"					>> remote.ent
	grep -e '^Package:' all-popcon-pkgs.txt | grep -f pkg.lst | $(DBIN)/popconent `cat all-popcon-submissions.txt`		>> remote.ent

# GENERATE
# source XML inclusion file updated for normal build (no remote access)
common.ent: Makefile debian/changelog
	echo "<!ENTITY build-date \"$(SOURCE_DATE)\">"			>  common.ent
	echo "<!ENTITY dr-version \"$(DR_VERSION)\">"			>> common.ent
	echo "<!ENTITY arch \"$(ARCH)\">"				>> common.ent
	echo "<!ENTITY codename-stable  \"$(RELEASE_NAME0L)\">"		>> common.ent
	echo "<!ENTITY Codename-stable  \"$(RELEASE_NAME0C)\">"		>> common.ent
	echo "<!ENTITY codename-testing  \"$(RELEASE_NAME1L)\">"	>> common.ent
	echo "<!ENTITY Codename-testing  \"$(RELEASE_NAME1C)\">"	>> common.ent
	echo "<!ENTITY codename-nexttesting  \"$(RELEASE_NAME2L)\">"	>> common.ent
	echo "<!ENTITY Codename-nexttesting  \"$(RELEASE_NAME2C)\">"	>> common.ent
	echo "<!ENTITY codename-unstable \"sid\">"			>> common.ent
	echo "<!ENTITY Codename-unstable \"Sid\">"                      >> common.ent
	echo "<!ENTITY release-outdated  \"$(RELEASE_NAMEXX)\">"	>> common.ent
	echo "<!ENTITY Codename-doc-1    \"$(RELEASE_DOC_1)\">"		>> common.ent
	echo "<!ENTITY Codename-doc-2    \"$(RELEASE_DOC_2)\">"		>> common.ent


#######################################################################
# Phase 2(build): RAWXML (template) -> English XML
#######################################################################

$(DRAW)/$(MANUAL).rawxml: $(SRC_XML)
	#echo $^
	cd $(DRAW) ; ./merge.sh

# Update URL list header from $(MANUAL).rawxml
$(DRAW)/header.rawxml: $(DRAW)/header1.rawxml $(DRAW)/header2.rawxml $(DRAW)/$(MANUAL).rawxml
	cat $(DRAW)/header1.rawxml >  $(DRAW)/header.rawxml
	$(XPNO) $(DXSL)/urls.xsl $(DRAW)/$(MANUAL).rawxml | sort | uniq |\
	sed -e "s/&/\&amp;/g"   |  fgrep -v -e '<!ENTITY  "">' >> $(DRAW)/header.rawxml
	cat $(DRAW)/header2.rawxml >> $(DRAW)/header.rawxml

# Replace table contents with @-@popcon*@-@ and @@@psize*@-@ and
# fix URL referencees and table ID.
$(MANUAL).en.xml: $(DRAW)/$(MANUAL).rawxml $(DRAW)/header.rawxml
	@$(call check-command, xsltproc, xsltproc)
	$(XPNO) $(DXSL)/table.xsl $(DRAW)/$(MANUAL).rawxml |\
	$(DBIN)/colspec.py  |\
	sed -e '/<!DOCTYPE /d' -e "1r $(DRAW)/header.rawxml" |\
	sed -e 's/@-@amp@-@/\&/g' -e 's/@-@\([^@]\+\)@-@/\&\1;/g' > $@

# Replace table contents with dummy text and
# fix URL references and table ID as the template for translation.
# This avoids bloated PO/POT files. (tablet.xsl used instead of table.xsl)
$(MANUAL).en.xmlt: $(DRAW)/$(MANUAL).rawxml $(DRAW)/header.rawxml
	# GENERATE $(MANUAL).en.xmlt (TEMPLATE to avoid bloated PO/POT files)
	@$(call check-command, xsltproc, xsltproc)
	# Note: tablet.xsl with tailing "t"
	$(XPNO) $(DXSL)/tablet.xsl $(DRAW)/$(MANUAL).rawxml |\
	$(DBIN)/colspec.py  |\
	sed -e '/<!DOCTYPE /d' -e "1r $(DRAW)/header.rawxml" |\
	sed -e 's/@-@amp@-@/\&/g' -e 's/@-@\([^@]\+\)@-@/\&\1;/g' > $@

#######################################################################
# Phase 3 build : English XML -> POT/PO/XML non-ENGLISH
#######################################################################
.PHONY: po pot
pot: $(DPO)/templates.pot
po: $(SRC_PO)

# Do not record line number to avoid useless diff in po/*.po files: --no-location
# Do not update templates.pot if contents are the same as before; -I '^"POT-Creation-Date:'
$(DPO)/templates.pot: $(MANUAL).en.xmlt FORCE
	@$(call check-command, po4a-gettextize, po4a)
	@$(call check-command, msgcat, gettext)
	$(GETTEXT) -m $(MANUAL).en.xmlt | \
sed -e 's,^"Content-Type: text/plain; charset=CHARSET\\n"$$,"Content-Type: text/plain; charset=UTF-8\\n",' |\
	$(MSGCAT) --no-location -o $(DPO)/templates.pot.new -
	if diff -I '^"POT-Creation-Date:' -q $(DPO)/templates.pot $(DPO)/templates.pot.new ; then \
	  echo "Don't update templates.pot" ;\
	  touch $(DPO)/templates.pot ;\
	  rm -f $(DPO)/templates.pot.new ;\
	else \
	  echo "Update templates.pot" ;\
	  mv -f $(DPO)/templates.pot.new $(DPO)/templates.pot ;\
	fi
	: > fuzzy.log

# Always update while dropping obsolete msgid/msgstr
$(DPO)/%.po: $(DPO)/templates.pot FORCE
	@$(call check-command, msgmerge, gettext)
	mv -f $(DPO)/$*.po $(DPO)/$*.po.orig
	$(UPDATEPO) $(DPO)/$*.po.orig $(DPO)/templates.pot
	$(MSGNOOBS) -o $(DPO)/$*.po $(DPO)/$*.po.orig
	rm -f $(DPO)/$*.po.orig
	$(DBIN)/fuzzypo $(DPO)/$*.po >>fuzzy.log

FORCE:
	@: "FORCE target"

# base XML files for all languages
.PHONY: xml
xml: $(addsuffix .xml, $(addprefix  $(MANUAL)., $(LANGALL)))

$(MANUAL).%.xml: $(DPO)/%.po $(MANUAL).en.xml
	@$(call check-command, po4a-translate, po4a)
	@$(call check-command, msgcat, gettext)
	$(DBIN)/genxml $*

#######################################################################
# Phase 4 build : Formatted conversion from XML
#######################################################################

#######################################################################
# $ make css       # update CSS and DIMG in $(BUILD_DIR)
#######################################################################
.PHONY: css
css:
	-rm -rf $(BUILD_DIR)/images
	mkdir -p $(BUILD_DIR)/images
	cp -f $(DXSL)/$(MANUAL).css $(BUILD_DIR)/$(MANUAL).css
	echo "AddCharset UTF-8 .txt" > $(BUILD_DIR)/.htaccess
	#cd $(DIMG) ; cp caution.png home.png important.png next.png note.png prev.png tip.png up.gif warning.png $(BUILD_DIR)/images
	cd $(DIMG) ; cp caution.png important.png note.png tip.png up.gif warning.png $(BUILD_DIR)/images
	cd png ; cp home.png next.png prev.png $(BUILD_DIR)/images

#######################################################################
# $ make html      # update all HTML in $(BUILD_DIR)
#######################################################################
.PHONY: html
html:	$(foreach LX, $(LANGALL), $(BUILD_DIR)/index.$(LX).html)

$(BUILD_DIR)/index.%.html: $(MANUAL).%.xml $(SRC_ENT)
	@$(call check-command, xsltproc, xsltproc)
	-mkdir -p $(BUILD_DIR)
	$(XPINC)   --stringparam base.dir $(BUILD_DIR)/ \
                --stringparam html.ext .$*.html \
                $(DXSL)/style-html.xsl $<

#######################################################################
# $ make txt       # update all Plain TEXT in $(BUILD_DIR)
#######################################################################
.PHONY: txt
txt:	$(foreach LX, $(LANGALL), $(BUILD_DIR)/$(MANUAL).$(LX).txt.gz)

# style-txt.xsl provides work around for hidden URL links by appending them explicitly.
$(BUILD_DIR)/$(MANUAL).%.txt.gz: $(MANUAL).%.xml $(SRC_ENT)
	@$(call check-command, w3m, w3m)
	@$(call check-command, xsltproc, xsltproc)
	-mkdir -p $(BUILD_DIR)
	@test -n /usr/bin/w3m  || { echo "ERROR: w3m not found. Please install the w3m package." ; false ;  }
	$(XPINC) $(DXSL)/style-txt.xsl $< | LC_ALL=en_US.UTF-8 \
	  w3m -o display_charset=UTF-8 -cols 70 -dump -no-graph -T text/html | \
	  gzip -n -9 - > $@

#######################################################################
# $ make pdf       # update all PDF in $(BUILD_DIR)
#######################################################################
.PHONY: pdf
pdf:	$(foreach LX, $(LANGALL), $(BUILD_DIR)/$(MANUAL).$(LX).pdf)

nopdf: $(BUILD_DIR)/nopdf.pdf

$(BUILD_DIR)/nopdf.pdf: nopdf.tex
	-mkdir -p $(BUILD_DIR)
	cd $(BUILD_DIR); \
	xelatex ../nopdf.tex

$(foreach LX, $(NOPDF), $(BUILD_DIR)/$(MANUAL).$(LX).pdf): $(BUILD_DIR)/nopdf.pdf
	-mkdir -p $(BUILD_DIR)
	cp $(BUILD_DIR)/nopdf.pdf $@

# dblatex.xsl provide work around for hidden URL links by appending them explicitly.
$(BUILD_DIR)/$(MANUAL).%.pdf: $(MANUAL).%.xml $(SRC_ENT)
	@$(call check-command, dblatex, dblatex)
	@$(call check-command, xsltproc, xsltproc)
	-mkdir -p $(BUILD_DIR)
	@test -n "$(DBLATEX)"  || { echo "ERROR: dblatex not found. Please install the dblatex package." ; false ;  }
	export TEXINPUTS=".:"; \
	$(XPINC) $(DXSL)/dblatex.xsl $<  | \
	$(DBLATEX) --style=native \
		--debug \
		--backend=xetex \
		--xsl-user=$(DXSL)/user_param.xsl \
		--xsl-user=$(DXSL)/xetex_param.xsl \
		--param=draft.mode=$(DRAFTMODE) \
		--param=lingua=$* \
		--output=$@ - || { echo "OMG!!!!!! XXX_CHECK_XXX ... Do not worry ..."; true ; }

#######################################################################
# $ make tex       # update all TeX source in $(BUILD_DIR)
#######################################################################
.PHONY: tex
tex:	$(foreach LX, $(LANGALL), $(BUILD_DIR)/$(MANUAL).$(LX).tex)

# dblatex.xsl provide work around for hidden URL links by appending them explicitly.
$(BUILD_DIR)/$(MANUAL).%.tex: $(MANUAL).%.xml $(SRC_ENT)
	-mkdir -p $(BUILD_DIR)
	@test -n "$(DBLATEX)"  || { echo "ERROR: dblatex not found. Please install the dblatex package." ; false ;  }
	export TEXINPUTS=".:"; \
	$(XPINC) $(DXSL)/dblatex.xsl $<  | \
	$(DBLATEX) --style=native \
		--debug \
		--type=tex \
		--backend=xetex \
		--xsl-user=$(DXSL)/user_param.xsl \
		--xsl-user=$(DXSL)/xetex_param.xsl \
		--param=draft.mode=$(DRAFTMODE) \
		--param=lingua=$* \
		--output=$@ - || { echo "OMG!!!!!! XXX_CHECK_XXX ... Do not worry ..."; true ; }

#######################################################################
# $ make epub      # update all epub in $(BUILD_DIR)
#######################################################################
.PHONY: epub
epub:	$(foreach LX, $(LANGALL), $(BUILD_DIR)/$(MANUAL).$(LX).epub)

$(BUILD_DIR)/$(MANUAL).%.epub: $(MANUAL).%.xml $(SRC_ENT)
	@$(call check-command, xsltproc, xsltproc)
	-mkdir -p $(BUILD_DIR)/$*/
	cd $(BUILD_DIR)/$*/ ; $(XPINC) $(CURDIR)/$(DXSL)/style-epub.xsl $(CURDIR)/$<
	cp -f $(DXSL)/mimetype $(BUILD_DIR)/$*/mimetype
	cp -f $(DXSL)/debian-reference.css $(BUILD_DIR)/$*/OEBPS/debian-reference.css
	cp -f $(DXSL)/debian-openlogo.png $(BUILD_DIR)/$*/OEBPS/debian-openlogo.png
	find  $(BUILD_DIR)/$*/ | xargs -n1 touch -d"$(SOURCE_DATE)"
	cd $(BUILD_DIR)/$*/ ; $(ZIP) --latest-time -r $@ ./

#######################################################################
# Phase 5 cleans
#######################################################################
#######################################################################
# $ make clean     # clean files ready for tar
#                  (override_dh_auto_clean call this)
#######################################################################
.PHONY: clean
clean: localclean remoteclean
.PHONY: distclean
distclean: clean
#######################################################################
# $ make localclean   # clean locally generated files
#######################################################################
.PHONY: localclean
localclean:
	# CLEAN
	-rm -rf $(BUILD_DIR)
	-rm -rf debian/tmp
	-rm -f *.swp *~ *.tmp
	-rm -f $(DPO)/*~ $(DPO)/*.mo $(DPO)/*.po.*
	-rm -f common.ent
	-rm -f $(addsuffix .xml, $(addprefix $(MANUAL)., $(LANGALL)))
	-rm -f $(addsuffix .xmlt, $(addprefix $(MANUAL)., $(LANGALL)))
	-rm -f $(DRAW)/$(MANUAL).rawxml
	-rm -f $(DRAW)/header.rawxml
	-rm -f $(DPO)/*.pot
	-rm -f fuzzy.log

#######################################################################
# $ make remoteclean # clean remote data to generate entity files
#######################################################################
.PHONY: remoteclean
remoteclean:
	-rm -f $(REMOTE_DATA)

#######################################################################
# $ make entityclean # clean entity files generated from remote data
#######################################################################
.PHONY: entityclean
entityclean:
	-rm -f remote.ent

#======================================================================
# ===== Utility targets ===============================================
#======================================================================

#######################################################################
# $ make wrap       # wrap all PO
#######################################################################
.PHONY: wrap nowrap wip replace
wrap:
	@$(call check-command, msgcat, gettext)
	for XX in $(foreach LX, $(LANGPO), $(DPO)/$(LX).po); do \
	$(MSGCAT) -o $$XX $$XX ;\
	done
nowrap:
	@$(call check-command, msgcat, gettext)
	for XX in $(foreach LX, $(LANGPO), $(DPO)/$(LX).po); do \
	$(MSGCAT) -o $$XX --no-wrap $$XX ;\
	done

replace:
	@$(call check-file, $(SEDF))
	@$(call check-command, msgcat, gettext)
	@$(call check-command, msguniq, gettext)
	for XX in $(foreach LX, $(LANGPO), $(DPO)/$(LX).po); do \
	$(MSGCAT) -o $$XX --no-wrap $$XX ;\
	done
	for XX in $(foreach LX, $(LANGPO), $(DPO)/$(LX).po); do \
	cat $$XX >$$XX.sed ;\
	$(SED) -f $(SEDF) <$$XX | sed -n -e '/^msgid "[^"]/,$$p' >>$$XX.sed ;\
	$(MSGUNIQ) -o $$XX.uniq $$XX.sed ;\
	done
	for XX in $(foreach LX, $(LANGPO), $(DPO)/$(LX).po); do \
	$(MSGCAT) -o $$XX $$XX.uniq ;\
	done

wip:
	@$(call check-command, msgattrib, gettext)
	for XX in $(foreach LX, $(LANGPO), $(DPO)/$(LX).po); do \
	$(MSGATTR) -o $$XX.fuzz --fuzzy        $$XX ;\
	$(MSGATTR) -o $$XX.untr --untranslated $$XX ;\
	done

#######################################################################
# $ make rsync
# export build result to http://people.debian.org/~osamu/debian-reference/
#######################################################################
.PHONY: rsync
rsync: all
	rsync -avz $(BUILD_DIR)/ osamu@people.debian.org:public_html/debian-reference/

#######################################################################
# $ make url       # check duplicate URL references
#######################################################################
.PHONY: url
url: $(MANUAL).en.xml
	@echo "----- Duplicate URL references (start) -----"
	-sed -ne "/^<\!ENTITY/s/<\!ENTITY \([^ ]*\) .*$$/\" \1 \"/p"  < $< | uniq -d | xargs -n 1 grep $< -e  | grep -e "^<\!ENTITY"
	@echo "----- Duplicate URL references (end) -----"
