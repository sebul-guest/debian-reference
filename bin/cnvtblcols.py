#!/usr/bin/python3
# vim: set sts=4 expandtab:

"""
input:
.title
[grid="all"]
`-------------`------------`------`--------`-----------------------------------------
file          permission   user   group    description
-------------------------------------------------------------------------------------
`/etc/passwd` `-rw-r--r--` `root` `root`   (sanitized) user account information
`/etc/shadow` `-rw-r-----` `root` `shadow` secure user account information
`/etc/group`  `-rw-r--r--` `root` `root`   group information
-------------------------------------------------------------------------------------

output:
.title
[width="100%", cols="20,12,6,12,50",grid="all",options="header"]
|=====================================================================================
|file          |permission   |user   |group    |description
|`/etc/passwd` |`-rw-r--r--` |`root` |`root`   |(sanitized) user account information
|`/etc/shadow` |`-rw-r-----` |`root` |`shadow` |secure user account information
|`/etc/group`  |`-rw-r--r--` |`root` |`root`   |group information
|=====================================================================================

"""

import sys, os, re, string

VERSION = '1.0.1'

def main():
    flag_table  = 0 # not in table
    flag_example  = 0 # not in example
    maxlength = 512
    index = 0
    for line in sys.stdin.readlines():
        line=line[0:-1]
        index += 1
        if flag_table == 0 and flag_example == 0 and re.match('\[grid', line):
            # pre-table header
            pass # do nothing
        elif flag_table == 0 and flag_example == 0 and re.match('`---', line):
            # 1st line in table
            flag_table   = 1 # now in ruler line of table
            i = 0 # sweeping pointer
            j = 0 # previous pointer
            pos = []
            column = 1
            for x in line:
                if x == '`' and i > 0:
                    pos.append((j, i))
                    column += 1
                    j = i
                i += 1
            pos.append((j, maxlength))
            if column == 1:
                print("ETC1: column = 1 at", index, file=sys.stderr)
                sys.exit(1)
            elif column == 2:
                print('[width="100%", cols="40,60", grid="all", options="header"]')
            elif column == 3:
                line = '[width="100%", cols="30,30,40", grid="all", options="header"]'
            elif column == 4:
                print('[width="100%", cols="20,12,6,62", grid="all", options="header"]')
            elif column == 5:
                print('[width="100%", cols="20,12,6,12,50", grid="all", options="header"]')
            elif column == 6:
                print('[width="100%", cols="20,12,6,12,12,38", grid="all", options="header"]')
            elif column == 7:
                print('[width="100%", cols="20,12,6,12,12,12,26", grid="all", options="header"]')
            else:
                print("ETC8: column > 7 at", index, file=sys.stderr)
                sys.exit(1)
            print('|==============================================================')
        elif flag_table == 1 and flag_example == 0:
            # 2nd line in table
            flag_table   = 2 # now in table header
            # process data entry
            xline = ""
            for x in pos:
                i, j = x
                if j < maxlength:
                    xline = xline + "|" + re.sub('\|', '@@@bar@@@',line[i:j].strip())
                else:
                    xline = xline + "|" + re.sub('\|', '@@@bar@@@',line[i:].strip())
            print(xline)
        elif flag_table == 2 and flag_example == 0:
            # 3rd line in table: do not output
            flag_table   = 3 # now in table header and contents separator
            if not re.match('----', line):
                print("ET4: should be table header and contents separator at", index, file=sys.stderr)
                sys.exit(1)
            pass # do nothing
        elif flag_table > 2 and flag_example == 0 and not re.match('----', line):
            # 4th line or later in table but not last
            flag_table   += 1 # now in table contents
            # process data entry
            xline = ""
            for x in pos:
                i, j = x
                if j < maxlength:
                    xline = xline + "|" + re.sub('\|', '@@@bar@@@',line[i:j])
                else:
                    xline = xline + "|" + re.sub('\|', '@@@bar@@@',line[i:])
            print(xline)
        elif flag_table > 2 and flag_example == 0 and re.match('----', line):
            # last line in table
            flag_table   = 0 # now out of table contents
            print('|==============================================================')
        elif flag_table == 0 and flag_example == 0 and re.match('----', line):
            flag_example = 1
            print('---------------------------------------------------------------')
        elif flag_table == 0 and flag_example == 1 and re.match('----', line):
            flag_example = 0
            print('---------------------------------------------------------------')
        elif flag_table == 0 and flag_example == 1:
            print(line.rstrip())
        elif flag_table == 0 and flag_example == 0:
            print(line.rstrip())
        else:
            print("ETX: strange at", index, flag_table, file=sys.stderr)
            sys.exit(1)

if __name__ == '__main__':
    main()
